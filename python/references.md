Python
=====


## TODO
- [ ] [Python v3.6 Tutorial](https://docs.python.org/3.6/tutorial/controlflow.html)
- [ ]https://github.com/jupyter/jupyter/wiki#general-python-programming
    -- Jupyter notebooks: Python for developer, Learning to code ...

# Notes
- [My Python intro notebook](intro-python.ipynb)
- [Notebook - Learn to Code with Python](Learn to Code with Python.ipynb)

# Books
- [Clean Python: Elegant codeing in python](https://learning.oreilly.com/library/view/clean-python-elegant/)
- [Magic universe - python daily prog by Anna-Lena Popkes](https://github.com/zotroneneis/magical_universe)
- [Talk Python podcast - 100 days of code](https://github.com/talkpython/100daysofcode-with-python-course)
- [Python koans](https://github.com/gregmalcolm/python_koans/) 
interactive tutorial for learning the Python programming language by making tests pass.

## Crash course
- [Google Python intro](https://developers.google.com/edu/python/introduction)
  -- https://developers.google.com/edu/python/exercises/basic
- [Python for developer - Ricardo](http://ricardoduarte.github.io/python-for-developers/#content)
   with jupyter nbview https://nbviewer.jupyter.org/github/ricardoduarte/python-for-developer
- [Learning to code with python - waterloo pygrp jupyternb](https://nbviewer.jupyter.org/urls/bitbucket.org/amjoconn/watpy-learning-to-code-with-python/raw/3441274a54c7ff6ff3e37285aafcbbd8cb4774f0/notebook/Learn%20to%20Code%20with%20Python.ipynb)
- [Python guide - Sharvin Shah. Brief intro to  decorators, ](https://www.freecodecamp.org/news/the-ultimate-guide-to-python-from-beginner-to-intermediate-to-pro/#kwargs-)


## Python data analytics
- https://jupyter.org/try -> Python: https://hub.mybinder.org/user/ipython-ipython-in-depth-47qc0nuy/notebooks/binder/Index.ipynb
- [Data Science Handbook - Jake VanderPlas](https://github.com/jakevdp/PythonDataScienceHandbook/blob/master/README.md)
- https://nbviewer.jupyter.org/gist/darribas/4121857
  More notebooks are at https://github.com/phelps-sg/python-bigdata
- [Python bigdata -python intro by S Phelps - jupyternb](https://nbviewer.jupyter.org/github/phelps-sg/python-bigdata/blob/master/src/main/ipynb/intro-python.ipynb)

